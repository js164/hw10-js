// 1. Используя метод CreateElement and append.
// 2. insertAdjacentHTML первый параметр это позиция куда будет вставлены данные, имеет 4 варианта: beforebegin, afterbegin, beforeend, afterend.
// 3. Можна удалить елемент через метод remove().

let arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

function createList(array, position) {
  array.forEach((elem) => {
    const item = document.createElement("li");
    item.innerText = elem;
    position.append(item);
    console.log(item);
  });
}

createList(arr, document.body);
